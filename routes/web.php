<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/view', 'UserController@index')->middleware('auth');
Route::get('/user/search', 'UserController@search')->middleware('auth');
Route::get('/user/{id}/konfirmasi', 'UserController@konfirmasiUser')->middleware('auth');
Route::resource('user','UserController')->middleware('auth');

Route::get('/tahun-ajaran/view', 'TahunAjaranController@index')->middleware('auth');
Route::get('/tahun-ajaran/all', 'TahunAjaranController@all')->middleware('auth');
Route::get('/tahun-ajaran/search', 'TahunAjaranController@search')->middleware('auth');
Route::resource('/tahun-ajaran','TahunAjaranController')->middleware('auth');

Route::get('/fakultas/view', 'FakultasController@index')->middleware('auth');
Route::get('/fakultas/all', 'FakultasController@all')->middleware('auth');
Route::get('/fakultas/search', 'FakultasController@search')->middleware('auth');
Route::resource('/fakultas','FakultasController')->middleware('auth');

Route::get('/jurusan/view', 'JurusanController@index')->middleware('auth');
Route::get('/jurusan/all', 'JurusanController@all')->middleware('auth');
Route::get('/jurusan/search', 'JurusanController@search')->middleware('auth');
Route::resource('/jurusan','JurusanController')->middleware('auth');

Route::get('/parameter/view', 'ParameterController@index')->middleware('auth');
Route::get('/parameter/all', 'ParameterController@all')->middleware('auth');
Route::get('/parameter/search', 'ParameterController@search')->middleware('auth');
Route::resource('/parameter','ParameterController')->middleware('auth');

Route::get('/subparameter/view', 'SubParameterController@index')->middleware('auth');
Route::get('/subparameter/all', 'SubParameterController@all')->middleware('auth');
Route::get('/subparameter/search', 'SubParameterController@search')->middleware('auth');
Route::resource('/subparameter','SubParameterController')->middleware('auth');

Route::get('/dosen/view', 'DosenController@index')->middleware('auth');
Route::get('/dosen/all', 'DosenController@all')->middleware('auth');
Route::get('/dosen/matkul', 'DosenController@dosenMatkul')->middleware('auth');
Route::get('/dosen/dosenParam', 'DosenController@dosenParam')->middleware('auth');
Route::get('/dosen/search', 'DosenController@search')->middleware('auth');
Route::resource('/dosen','DosenController')->middleware('auth');

Route::get('/mahasiswa/view', 'MahasiswaController@index')->middleware('auth');
Route::get('/mahasiswa/all', 'MahasiswaController@all')->middleware('auth');
Route::get('/mahasiswa/search', 'MahasiswaController@search')->middleware('auth');
Route::resource('/mahasiswa','MahasiswaController')->middleware('auth');

Route::get('/mata-kuliah/view', 'MataKuliahController@index')->middleware('auth');
Route::get('/mata-kuliah/all', 'MataKuliahController@all')->middleware('auth');
Route::get('/mata-kuliah/search', 'MataKuliahController@search')->middleware('auth');
Route::resource('/mata-kuliah','MataKuliahController')->middleware('auth');

Route::post('/institusi/search', 'UserController@search_institusi');

Route::resource('/profile','ProfileController')->middleware('auth');

Route::post('/profile-foto','ProfileController@uploadFoto')->middleware('auth');
Route::resource('/profile-dosen','ProfileDosenController')->middleware('auth');

Route::get('/password/cari','UbahPasswordController@cari')->middleware('auth');
Route::resource('/ubah/password','UbahPasswordController')->middleware('auth');

Route::post('/register/autocomplete', 'Auth\RegisterController@getInstitusi')->name('registerAutocomplete');
Route::get('/register/tahun_ajaran', 'Auth\RegisterController@getTahunAjaran')->name('getTahunAjaran');
Route::get('/register/fakultas', 'Auth\RegisterController@getFakultas')->name('getFakultas');
Route::post('/register/jurusan', 'Auth\RegisterController@getJurusan')->name('getJurusan');
Route::post('/register/luar', 'Auth\RegisterController@create');

Route::get('/input/view', 'InputController@index')->middleware('auth');
Route::get('/input/all', 'InputController@all')->middleware('auth');
Route::get('/input/search', 'InputController@search')->middleware('auth');
Route::resource('/input','InputController')->middleware('auth');

