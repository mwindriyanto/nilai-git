<?php

use Illuminate\Database\Seeder;
use App\Jurusan;

class JurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jurusan::create(['kode' => 'TEI' , 'jurusan' => 'S1 Teknik Informatika', 'id_fakultas' => 3]);
        Jurusan::create(['kode' => 'TIF' , 'jurusan' => 'S1 Teknologi Informasi', 'id_fakultas' => 3]);
        Jurusan::create(['kode' => 'SI' , 'jurusan' => 'S1 Sistem Informasi', 'id_fakultas' => 3]);
        Jurusan::create(['kode' => 'TIN' , 'jurusan' => 'S1 Teknik Industri', 'id_fakultas' => 3]);
        Jurusan::create(['kode' => 'MI' , 'jurusan' => 'D3 Manajemen Informatika', 'id_fakultas' => 3]);
    }
}
