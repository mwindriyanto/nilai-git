<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['email' => 'mwindriyanto@gmail.com','name' => 'admin','password' => bcrypt('bismillah'),'konfirmasi_admin' => 1]);
        User::create(['email' => 'wahyu@unjaya.ac.id','name' => 'Wahyu', 'password' => bcrypt('bismillah'),'konfirmasi_admin' => 1]);
        User::create(['email' => 'mahasiswa@gmail.com','name' => 'Mahasiswa', 'password' => bcrypt('bismillah'),'konfirmasi_admin' => 0]);
        //
    }
}
