<?php

use Illuminate\Database\Seeder;
use App\Fakultas;

class FakultasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fakultas::create(['kode' => 'FK' , 'fakultas' => 'Fakultas Kesehatan']);
        Fakultas::create(['kode' => 'FES' , 'fakultas' => 'Fakultas Ekonomi dan Sosial']);
        Fakultas::create(['kode' => 'FTTI' , 'fakultas' => 'Fakultas Tekmik dan Teknologi Informasi']);
    }
}
