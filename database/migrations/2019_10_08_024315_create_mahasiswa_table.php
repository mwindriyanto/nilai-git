<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_jurusan')->unsigned();
            $table->integer('id_tahun_ajaran')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->integer('semester')->default(0);
            $table->string('nama_depan');
            $table->string('nama_belakang');
            $table->string('nim')->unique();
            $table->string('alamat');
            $table->string('nohp');
            $table->timestamps();
            $table->foreign('id_jurusan')->references('id')->on('jurusan');
            $table->foreign('id_tahun_ajaran')->references('id')->on('tahun_ajaran');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa');
    }
}
