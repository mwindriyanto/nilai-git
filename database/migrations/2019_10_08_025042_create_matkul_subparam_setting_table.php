<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatkulSubparamSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matkul_subparam_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_matkul')->unsigned();
            $table->integer('id_sub_parameter')->unsigned();
            $table->integer('id_tahun_ajaran')->unsigned();
            $table->timestamps();
            $table->foreign('id_matkul')->references('id')->on('mata_kuliah');
            $table->foreign('id_sub_parameter')->references('id')->on('sub_parameter_nilai');
            $table->foreign('id_tahun_ajaran')->references('id')->on('tahun_ajaran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matkul_subparam_setting');
    }
}
