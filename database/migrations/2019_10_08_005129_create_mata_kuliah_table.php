<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMataKuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_kuliah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_jurusan')->unsigned();
            $table->integer('id_tahun_ajaran')->unsigned();
            $table->integer('id_dosen')->unsigned();
            $table->string('kode')->unique();
            $table->string('mata_kuliah');
            $table->timestamps();
            $table->foreign('id_jurusan')->references('id')->on('jurusan');
            $table->foreign('id_dosen')->references('id')->on('users');
            $table->foreign('id_tahun_ajaran')->references('id')->on('tahun_ajaran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mata_kuliah');
    }
}
