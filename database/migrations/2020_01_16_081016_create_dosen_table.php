<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nidn')->unique();
            $table->string('nama');
            $table->integer('id_tahun_ajaran')->unsigned();
            $table->integer('id_fakultas')->unsigned();
            $table->integer('id_jurusan')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->string('no_wa')->default('-');
            $table->text('alamat')->default(null);
            $table->timestamps();
            $table->foreign('id_tahun_ajaran')->references('id')->on('tahun_ajaran');
            $table->foreign('id_fakultas')->references('id')->on('fakultas');
            $table->foreign('id_jurusan')->references('id')->on('jurusan');
            $table->foreign('id_user')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosen');
    }
}
