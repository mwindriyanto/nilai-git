<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubParameterNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_parameter_nilai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_parameter')->unsigned();
            $table->string('sub_parameter');
            $table->integer('prosentase')->default(0);
            $table->timestamps();
            $table->foreign('id_parameter')->references('id')->on('parameter_nilai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_parameter_nilai');
    }
}
