<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_nilai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dosen',10)->unsigned();
            $table->integer('id_mata_kuliah',10)->unsigned();
            $table->string('parameter');
            $table->integer('prosentase')->default(0);
            $table->timestamps();
            $table->foreign('id_dosen')->references('id')->on('dosen');
            $table->foreign('id_mata_kuliah')->references('id')->on('mata_kuliah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_nilai');
    }
}
