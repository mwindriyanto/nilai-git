<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaiMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_mahasiswa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tahun_ajaran')->unsigned();
            $table->integer('id_mahasiswa')->unsigned();
            $table->integer('id_mss')->unsigned();
            $table->integer('nilai');
            $table->integer('semester')->default(0);
            $table->timestamps();
            $table->foreign('id_tahun_ajaran')->references('id')->on('tahun_ajaran');
            $table->foreign('id_mahasiswa')->references('id')->on('mahasiswa');
            $table->foreign('id_mss')->references('id')->on('matkul_subparam_setting');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_mahasiswa');
    }
}
