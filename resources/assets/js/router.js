
// 1. Define route components.
// These can be imported from other files
import DashboardIndex from './components/dashboard/DashboardIndex.vue';
import UserIndex from './components/user/UserIndex.vue';
import UserCreate from './components/user/UserCreate.vue';
import UserEdit from './components/user/UserEdit.vue';
import TahunAjaranIndex from './components/tahun-ajaran/TahunAjaranIndex.vue';
import TahunAjaranCreate from './components/tahun-ajaran/TahunAjaranCreate.vue';
import TahunAjaranEdit from './components/tahun-ajaran/TahunAjaranEdit.vue';
import FakultasIndex from './components/fakultas/FakultasIndex.vue';
import FakultasCreate from './components/fakultas/FakultasCreate.vue';
import FakultasEdit from './components/fakultas/FakultasEdit.vue';
import JurusanIndex from './components/jurusan/JurusanIndex.vue';
import JurusanCreate from './components/jurusan/JurusanCreate.vue';
import JurusanEdit from './components/jurusan/JurusanEdit.vue';

import ParameterIndex from './components/parameter/ParameterIndex.vue';
import ParameterCreate from './components/parameter/ParameterCreate.vue';
import ParameterEdit from './components/parameter/ParameterEdit.vue';
import SubParameterIndex from './components/subparameter/SubParameterIndex.vue';
import SubParameterCreate from './components/subparameter/SubParameterCreate.vue';
import SubParameterEdit from './components/subparameter/SubParameterEdit.vue';

import DosenIndex from './components/dosen/DosenIndex.vue';
import DosenCreate from './components/dosen/DosenCreate.vue';
import DosenEdit from './components/dosen/DosenEdit.vue';
import MahasiswaIndex from './components/mahasiswa/MahasiswaIndex.vue';
import MahasiswaCreate from './components/mahasiswa/MahasiswaCreate.vue';
import MahasiswaEdit from './components/mahasiswa/MahasiswaEdit.vue';

import MataKuliahIndex from './components/matakuliah/MataKuliahIndex.vue';
import MataKuliahCreate from './components/matakuliah/MataKuliahCreate.vue';
import MataKuliahEdit from './components/matakuliah/MataKuliahEdit.vue';

import ProfileIndex from './components/profile/ProfileIndex.vue';
import ProfileEdit from './components/profile/ProfileEdit.vue'; 
import ProfileFoto from './components/profile/ProfileFoto.vue'; 

import PasswordUbah from './components/ubah-password/UbahPassword.vue';

import InputIndex from './components/input/InputIndex.vue';

import Page404 from './components/error/Page404.vue';

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/', component: DashboardIndex,name:'IndexDashboard' },
  { path: '/user', component: UserIndex,name: 'IndexUser' },
  { path: '/user/create', component: UserCreate,name: 'CreateUser' },
  { path: '/user/edit/:id', component: UserEdit,name: 'EditUser' },

  { path: '/tahun-ajaran', component:TahunAjaranIndex,name: 'IndexTahunAjaran' },
  { path: '/tahun-ajaran/create', component:TahunAjaranCreate,name: 'CreateTahunAjaran' },
  { path: '/tahun-ajaran/edit/:id', component:TahunAjaranEdit,name: 'EditTahunAjaran' },

  { path: '/fakultas', component:FakultasIndex,name: 'IndexFakultas' },
  { path: '/fakultas/create', component:FakultasCreate,name: 'CreateFakultas' },
  { path: '/fakultas/edit/:id', component:FakultasEdit,name: 'EditFakultas' },

  { path: '/jurusan', component:JurusanIndex,name: 'IndexJurusan' },
  { path: '/jurusan/create', component:JurusanCreate,name: 'CreateJurusan' },
  { path: '/jurusan/edit/:id', component:JurusanEdit,name: 'EditJurusan' },

  { path: '/parameter', component:ParameterIndex,name: 'IndexParameter' },
  { path: '/parameter/create', component:ParameterCreate,name: 'CreateParameter' },
  { path: '/parameter/edit/:id', component:ParameterEdit,name: 'EditParameter' },

  { path: '/subparameter', component:SubParameterIndex,name: 'IndexSubParameter' },
  { path: '/subparameter/create', component:SubParameterCreate,name: 'CreateSubParameter' },
  { path: '/subparameter/edit/:id', component:SubParameterEdit,name: 'EditSubParameter' },
  { path: '/mahasiswa', component: MahasiswaIndex, name: 'IndexMahasiswa'},
  { path: '/mahasiswa/create', component: MahasiswaCreate, name: 'CreateMahasiswa'},
  { path: '/mahasiswa/edit/:id', component: MahasiswaEdit, name: 'EditMahasiswa'},
  { path: '/mata-kuliah', component: MataKuliahIndex, name: 'IndexMataKuliah'},
  { path: '/mata-kuliah/create', component: MataKuliahCreate, name: 'CreateMataKuliah'},
  { path: '/mata-kuliah/edit/:id', component: MataKuliahEdit, name: 'EditMataKuliah'},
  { path: '/profile', component: ProfileIndex, name: 'IndexProfile'},
  { path: '/profile/edit/:id', component: ProfileEdit, name: 'EditProfile'},
  { path: '/profile-foto', component: ProfileFoto, name: 'FotoProfile'},
  { path: '/*', component: Page404 },
  { path: '/dosen', component:DosenIndex,name:'IndexDosen'},
  { path: '/dosen/create', component:DosenCreate,name: 'CreateDosen' },
  { path: '/dosen/edit/:id', component:DosenEdit,name: 'EditDosen' },

  { path: '/password/ubah', component:PasswordUbah,name: 'UbahPassword' },

  { path: '/input', component: InputIndex,name:'IndexInput' },
]

export default routes;
