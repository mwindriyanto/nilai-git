@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/register/luar">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('nidn') ? ' has-error' : '' }}">
                            <label for="nidn" class="col-md-4 control-label">NIDN</label>

                            <div class="col-md-6">
                                <input id="nidn" type="text" class="form-control" name="nidn" value="{{ old('nidn') }}" required autofocus>

                                @if ($errors->has('nidn'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nidn') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nama Lengkap</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('institusi') ? ' has-error' : '' }}">
                            <label for="institusi" class="col-md-4 control-label">Institusi</label>

                            <div class="col-md-6">
                                <input id="institusi" type="text" class="form-control" name="institusi" value="{{ old('institusi') }}" onkeyup="getInstitusi()" required autofocus>
                                <input id="id_institusi" type="text" class="form-control" name="id_institusi" value="{{ old('institusi') }}" style="display:none">
                                <div id="list_institusi">
                                    
                                </div>
                                @if ($errors->has('institusi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('institusi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Profesi</label>
                            <div class="col-md-6">
                                <select class="form-control" required="" id="profesi" name="profesi">
                                  <option value="">-Pilih Profesi-</option>
                                  <option value="dosen">Dosen</option>
                                  <option value="mahasiswa">Mahasiswa</option>
                                </select>
                                @if ($errors->has('profesi  '))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profesi  ') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tahun Ajaran</label>
                            <div class="col-md-6">
                                <select class="form-control" required="" id="tahun_ajaran" name="tahun_ajaran">
                                    {{-- get Data Ajax --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Fakultas</label>
                            <div class="col-md-6">
                                <select class="form-control" required="" id="fakultas" name="fakultas" onchange="getJurusan()">
                                    {{-- get Data Ajax --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Jurusan</label>
                            <div class="col-md-6">
                                <select class="form-control" required="" id="jurusan" name="jurusan">
                                    <option value="">-Pilih Jurusan-</option>
                                    {{-- get Data Ajax --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('no_hp') ? ' has-error' : '' }}">
                            <label for="no_hp" class="col-md-4 control-label">Nomor HP</label>
                            <div class="col-md-6">
                                <input id="no_hp" type="text" class="form-control" name="no_hp" required>
                                @if ($errors->has('no_hp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_hp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                            <label for="alamat" class="col-md-4 control-label">Alamat</label>
                            <div class="col-md-6">
                                <input id="alamat" type="text" class="form-control" name="alamat" required>
                                @if ($errors->has('alamat'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('alamat') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
    function getInstitusi(){
        let institusi=document.getElementById('institusi').value;
        if(institusi != ''){
            let _token=$('input[name="_token"]').val();
            $.ajax({
               url:"{{route('registerAutocomplete')}}",
               method:"POST",
               data:{
                   institusi:institusi,
                   _token:_token
               },
               success:function(data){
                   
                   if(data!=0)
                   {
                        $('#list_institusi').fadeIn();
                        $('#list_institusi').html(data);
                   }
               } 
            });
        }else{
            $('#list_institusi').hide();
        }
    }

    function tempel(data){
        console.log(data);
        $('#id_institusi').val(data);
    }

    function getJurusan()
    {
        let fakultas=$('#fakultas').val();
        let _token=$('input[name="_token"]').val();
        $.ajax({
            url:"{{route('getJurusan')}}",
            method:'POST',
            data : {
                fak:fakultas,
                _token:_token
            },
            success:function(data){
                $('#jurusan').html(data);
            } 
        });
    }

    
    
    $(document).on('click','li',function(){
        $('#institusi').val($(this).text());
        $('#list_institusi').fadeOut();
    });

    $(document).ready(function(){
        
        let _token=$('input[name="_token"]').val();
        // get Tahun Ajaran
        $.ajax({
            url:"{{route('getTahunAjaran')}}",
            method:'GET',
            
            success:function(data){
                console.log(data);
                $('#tahun_ajaran').html(data);
            } 
        });

        // get Fakultas
        $.ajax({
            url:"{{route('getFakultas')}}",
            method:'GET',
            
            success:function(data){
                console.log(data);
                $('#fakultas').html(data);
            } 
        });
        

        
        
    });
</script>
@endsection
