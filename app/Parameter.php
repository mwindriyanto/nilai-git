<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $table = "parameter";
    protected $fillable = ['id_dosen','id_mata_kuliah','parameter','prosentase'];
}
