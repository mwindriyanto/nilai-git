<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    protected $table = 'mata_kuliah';
    protected $fillable = ['id_jurusan', 'id_tahun_ajaran', 'id_dosen', 'kode', 'mata_kuliah'];
}
