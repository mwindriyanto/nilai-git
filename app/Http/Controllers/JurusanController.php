<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Jurusan;

class JurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return DB::table('jurusan')
               ->leftJoin('fakultas',
               'jurusan.id_fakultas','=','fakultas.id')
               ->select('jurusan.*','fakultas.fakultas as fakultas')
               ->paginate(10);
    }
    
    public function all()
    {
        //
        return Jurusan::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_fakultas' => 'required|numeric',
            'kode' => 'required|unique:jurusan,kode|max:255',
            'jurusan' => 'required|unique:jurusan,jurusan|max:255',
        ]);
        $jurusan = DB::table('jurusan')->insert($request->all());
    }

    public function search(Request $request){
       return Jurusan::where('kode','LIKE',"%$request->q%")    
        ->orWhere('jurusan','LIKE',"%$request->q%")->paginate(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return DB::table('jurusan')
               ->leftJoin('fakultas',
               'jurusan.id_fakultas','=','fakultas.id')
               ->select('jurusan.*','fakultas.fakultas as fakultas')
               ->where('jurusan.id',$id)
               ->get();
        // return Jurusan::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'id_fakultas' => 'required|numeric',
            'kode' => 'required|unique:jurusan,kode,'.$id.'|max:255',
            'jurusan' => 'required|unique:jurusan,jurusan,'.$id.'|max:255',
        ]);
        $jurusan = Jurusan::find($id)->update($request->all());
        if($jurusan) {
           return response(200);
        } else {
           return response(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $jurusan = Jurusan::destroy($id);
        if($jurusan){
          return response(200);
        } else {
          return response(500);    
        }
    }
}
