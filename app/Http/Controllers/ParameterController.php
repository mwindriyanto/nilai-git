<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parameter;
use App\Dosen;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_dosen=Auth::user()->id;
        $parameter = DB::table('parameter')
        ->leftjoin('mata_kuliah','parameter.id_mata_kuliah','=','mata_kuliah.id')
        ->leftjoin('jurusan','mata_kuliah.id_jurusan','=','jurusan.id')
        ->select('parameter.*', 'mata_kuliah.mata_kuliah','jurusan.jurusan')
        ->where('parameter.id_dosen',$id_dosen)
        ->paginate(10);
        return $parameter;
    }
    
    public function all()
    {
        //
        return Parameter::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'parameter' => 'required|unique:parameter,parameter|max:255',
            'prosentase' => 'required|numeric|max:100',
        ]);
        $getDosen=Dosen::where('id_user',$request->id_dosen)->first();
        $parameter = Parameter::create([
            'id_dosen'=> $getDosen->id,
            'id_mata_kuliah'=> $request->id_matkul,
            'parameter'=> $request->parameter,
            'prosentase'=> $request->prosentase
        ]);
        
        // return $request->id_matkul;
    }

    public function search(Request $request){
    //    return Parameter::where('parameter','LIKE',"%$request->q%")    
    //     ->orWhere('parameter','LIKE',"%$request->q%")->paginate(10);
        $id_dosen=DB::table('dosen')->where('id_user',Auth::user()->id)->first();
        $parameter = DB::table('parameter')
        ->join('mata_kuliah','parameter.id_mata_kuliah','=','mata_kuliah.id')
        ->select('parameter.*', 'mata_kuliah.mata_kuliah')
        ->where('parameter.id_dosen',$id_dosen->id)
        ->where('mata_kuliah.mata_kuliah','LIKE',"%$request->q%")
        ->orwhere('parameter','LIKE',"%$request->q%")
        ->paginate(10);
        return $parameter;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // return Parameter::find($id);
        // return $id;
        $data=DB::table('parameter')
            ->leftJoin('mata_kuliah','parameter.id_mata_kuliah','=','mata_kuliah.id')
            ->leftJoin('jurusan','mata_kuliah.id_jurusan','=','jurusan.id')
            ->select('parameter.*','mata_kuliah.mata_kuliah','jurusan.jurusan')
            ->where('parameter.id',$id)
            ->get();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'parameter' => 'required|unique:parameter,parameter,'.$id.'|max:255',
            'prosentase' => 'required|numeric|max:100',
            'id_mata_kuliah' => 'required'
        ]);
        // $parameter = Parameter::find($id)->update($request->all());
        $parameter = DB::table('parameter')->where('id',$id)->update([
            'id_mata_kuliah'=>$request->id_mata_kuliah,
            'parameter'=>$request->parameter,
            'prosentase'=>$request->prosentase,
        ]);
        if($parameter) {
           return response(200);
        } else {
           return response(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $parameter = Parameter::destroy($id);
        if($parameter){
          return response(200);
        } else {
          return response(500);    
        }
    }
}
