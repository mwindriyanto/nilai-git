<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Dosen;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       return  User::paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function search(Request $request){
     
     $findUser = User::where('email','LIKE',"%$request->q%")->orWhere('name','LIKE',"%$request->q")->paginate(10);
     return $findUser;

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama' => 'required',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|same:confirm_password',
            'confirm_password' => 'required',

        ]);
        $user=User::create([
            'name' => $request->nama,
            'email' => $request->email,
            'password' => hash::make($request->password),
            'is_dosen' => 1
        ]);
        if($user){
          return response(200);    
        } else {
          return response(500);    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $user = User::find($id);

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'email' => 'required|unique:users,email,'.$id.'|max:255|email',
            'name' => 'required',
        ]);

        $updateUser = User::find($id)->update(['email' => $request->email,'name' => $request->name]);
        if($updateUser){
         return response(200);    
        } else {
         return response(500);    
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       $deleteUser =  User::destroy($id);
       if($deleteUser){
         return response(200);    
       } else {
         return response(500);    
       }
    }

    public function konfirmasiUser($id){
        
       $konfirmasiUser = User::find($id)->update(['konfirmasi_admin' => 1]);
       if($konfirmasiUser) {
          return response(200);    
       } else {
          return response(500);    
       }
    }

    public function search_institusi(Request $request)
    {
        // return $request->institusi;
        $data = null;
        if($request->institusi=="" || $request->institusi==null)
        {
            return $data;
        }
        else{
            $data = DB::table('institusi')->where('institusi','LIKE',"%$request->institusi%")->get();
            return $data;
        }
    }
}
