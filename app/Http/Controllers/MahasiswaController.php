<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mahasiswa;
use App\User;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::table('mahasiswa')->join('jurusan','mahasiswa.id_jurusan','=','jurusan.id')
                ->join('tahun_ajaran', 'mahasiswa.id_tahun_ajaran','=','tahun_ajaran.id')
                ->select('mahasiswa.*', 'jurusan', 'tahun_ajaran')
                ->paginate(10);
    }

    public function search(Request $request){
        return DB::table('mahasiswa')->join('jurusan','mahasiswa.id_jurusan','=','jurusan.id')
                ->join('tahun_ajaran', 'mahasiswa.id_tahun_ajaran','=','tahun_ajaran.id')
                ->select('mahasiswa.*', 'jurusan', 'tahun_ajaran')
                ->where('mahasiswa.nama_depan','LIKE', "%$request->q%")
                ->orWhere('mahasiswa.nama_belakang','LIKE', "%$request->q%")
                ->orWhere('mahasiswa.nim','LIKE', "%$request->q%")
                ->paginate(10);
     }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_jurusan' => 'required', 
            'id_tahun_ajaran' => 'required', 
            'nama_depan' => 'required', 
            'nama_belakang' => 'required', 
            'nim' => 'required', 
            'alamat' => 'required', 
            'nohp' => 'required',
            'email' => 'required|unique:users|max:255|email',
        ]);
        
        User::create([
            'email' => $request->email,
            'name' => $request->nama_depan." ".$request->nama_belakang,
            'password' => bcrypt('rahasia'),
            'konfirmasi_admin' => 0,
            'is_mhs' => 1
        ]);
        
        $email = $request->email;
        $data = DB::table('users')->where('email', $email)->first();

        Mahasiswa::create([
            'id_jurusan' => $request->id_jurusan, 
            'id_tahun_ajaran' => $request->id_tahun_ajaran, 
            'nama_depan' => $request->nama_depan, 
            'nama_belakang' => $request->nama_belakang, 
            'nim' => $request->nim, 
            'alamat' => $request->alamat, 
            'nohp' => $request->nohp,
            'id_user' => $data->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mahasiswa = DB::select('SELECT m.*, id_fakultas, email FROM mahasiswa m 
                    LEFT JOIN jurusan j ON m.id_jurusan = j.id
                    LEFT JOIN fakultas f ON f.id = j.id_fakultas
                    LEFT JOIN users u ON u.id = m.id_user 
                    WHERE m.id = ?', [$id]);

        return $mahasiswa;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_jurusan' => 'required', 
            'id_tahun_ajaran' => 'required', 
            'nama_depan' => 'required', 
            'nama_belakang' => 'required', 
            'nim' => 'required', 
            'alamat' => 'required', 
            'nohp' => 'required',
            'email' => 'required|max:255|email',
        ]);

        $mahasiswa = Mahasiswa::where('id', $id)->update([
            'id_jurusan' => $request->id_jurusan, 
            'id_tahun_ajaran' => $request->id_tahun_ajaran, 
            'nama_depan' => $request->nama_depan, 
            'nama_belakang' => $request->nama_belakang, 
            'nim' => $request->nim, 
            'alamat' => $request->alamat, 
            'nohp' => $request->nohp
        ]);

        $user = User::where('id', $request->id_user)->update([
            'name' => $request->nama_depan." ".$request->nama_belakang,
            'email' => $request->email
        ]);

        if($mahasiswa && $user){
            return response(200);    
        } else {
            return response(500);    
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mahasiswa = Mahasiswa::destroy($id);
        if($mahasiswa){
          return response(200);
        } else {
          return response(500);    
        }
    }
}
