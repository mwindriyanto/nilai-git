<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return Dosen::paginate(10);
        return DB::table('dosen')->join('jurusan','dosen.id_jurusan','=','jurusan.id')
                ->join('tahun_ajaran', 'dosen.id_tahun_ajaran','=','tahun_ajaran.id')
                ->join('fakultas', 'dosen.id_fakultas','=','fakultas.id')
                ->select('dosen.*', 'jurusan', 'tahun_ajaran','fakultas')
                ->paginate(10);
    }
    
    public function all()
    {
        //
        return Dosen::all();
    }

    public function dosenMatkul()
    {
        
        $user=Auth::user()->id;
        $data = User::find($user);
        return $data;
    }

    public function dosenParam()
    {
        $user = Auth::user()->id;
        $data=Dosen::where('id_user',$user)->first();
        return $data;
    }

    public function search(Request $request)
    {
        return DB::table('dosen')->join('jurusan','dosen.id_jurusan','=','jurusan.id')
                ->join('tahun_ajaran', 'dosen.id_tahun_ajaran','=','tahun_ajaran.id')
                ->join('fakultas', 'dosen.id_fakultas','=','fakultas.id')
                ->select('dosen.*', 'jurusan', 'tahun_ajaran','fakultas')
                ->where('dosen.nama','LIKE', "%$request->q%")
                ->orWhere('dosen.nidn','LIKE', "%$request->q%")
                ->paginate(10);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nidn' => 'required',
            'nama' => 'required',
            'institusi' => 'required',
            'id_fakultas' =>'required',
            'id_jurusan'=>'required',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required',
            'no_hp'=>'required|numeric',
            'alamat'=>'required',

        ]);
        $createUser = User::create([
            'name' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'konfirmasi_admin' => 0,
            'is_dosen' => 1,
        ]);
        $id = DB::table('users')->where('email',$request->email)->first();
        $createDosen = new Dosen;
            $createDosen->nidn = $request->nidn;
            $createDosen->nama = $request->nama;
            $createDosen->id_institusi = $request->id_institusi;
            $createDosen->id_tahun_ajaran=$request->id_tahun_ajaran;
            $createDosen->id_fakultas=$request->id_fakultas;
            $createDosen->id_jurusan=$request->id_jurusan;
            $createDosen->id_user=$id->id;
            $createDosen->no_wa=$request->no_hp;
            $createDosen->alamat=$request->alamat;
        $createDosen->save();
        
        if($createDosen){
          return response(200);    
        } else {
          return response(500);    
        }
        
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $dosen = Dosen::find($id);
        return DB::table('dosen')
            ->join('users','dosen.id_user','=','users.id')
            ->select('dosen.*', 'users.name','users.email')
            ->where('dosen.id',$id)
            ->get();
        // return $dosen->id;
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nidn' => 'required|max:255',
            'nama' => 'required|max:255',
            'id_tahun_ajaran' => 'required',
            'id_fakultas' => 'required',
            'id_jurusan' => 'required',
            'email' => 'required|unique:dosen,nidn|email|max:255',
            'no_wa' => 'required',
            'alamat' => 'required',
        ]);
        $data = Dosen::find($id);
        $dosen = Dosen::find($id)->update([
            'nidn' => $request->nidn,
            'nama' => $request->nama,
            'id_tahun_ajaran' => $request->id_tahun_ajaran,
            'id_fakultas' => $request->id_fakultas,
            'id_jurusan' => $request->id_jurusan,
            'no_wa' => $request->no_wa,
            'alamat' => $request->alamat,
        ]);
        $createUser = User::find($data->id_user)->update(['email' => $request->email,
                                   'name' => $request->nama]);

        if($dosen) {
           return response(200);
        } else {
           return response(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $dosen = Dosen::destroy($id);
        if($dosen){
          return response(200);
        } else {
          return response(500);    
        }
    }
}
