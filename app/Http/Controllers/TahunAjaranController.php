<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TahunAjaran;

class TahunAjaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TahunAjaran::paginate(10);
    }

    public function all()
    {
        return TahunAjaran::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function search(Request $request){
     
        $findTahunAjaran = TahunAjaran::where('tahun_ajaran','LIKE',"%$request->q%")->paginate(10);
        return $findTahunAjaran;
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun_ajaran' => 'required'
        ]);
        
        TahunAjaran::create([
            'tahun_ajaran' => $request->tahun_ajaran
        ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tahunAjaran = TahunAjaran::find($id);

        return $tahunAjaran;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'tahun_ajaran' => 'required',
        ]);

        $updateTahunAjaran = TahunAjaran::where('id', $id)->update([
            'tahun_ajaran' => $request->tahun_ajaran
        ]);

        if($updateTahunAjaran){
            return response(200);    
        } else {
            return response(500);    
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tahunAjaran = TahunAjaran::destroy($id);
        if($tahunAjaran){
          return response(200);
        } else {
          return response(500);    
        }
    }
}
