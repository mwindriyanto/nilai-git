<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mahasiswa;
use App\Dosen;
use App\User;
use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $idUser = $user->id;
        if($user->is_mhs == 1){
            return  DB::table('users')->leftJoin('mahasiswa','mahasiswa.id_user','=','users.id')
                    ->select('name','email','konfirmasi_admin','is_admin','is_dosen','is_mhs',
                             'users.created_at as created_user','mahasiswa.*')
                    ->where('users.id', $idUser)->get();
        }else if($user->is_dosen == 1){
            return  DB::table('users')->leftJoin('dosen','dosen.id_user','=','users.id')
                    ->select('name','email','konfirmasi_admin','is_admin','is_dosen','is_mhs',
                             'users.created_at as created_user','dosen.*')
                    ->where('users.id', $idUser)->get();
        }else{
            return $user = array(
                'is_admin' => 1,
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $idUser = $user->id;
        if($user->is_mhs == 1){
            return  DB::table('users')->leftJoin('mahasiswa','mahasiswa.id_user','=','users.id')
                    ->leftJoin('jurusan', 'jurusan.id', '=', 'mahasiswa.id_jurusan')
                    ->leftJoin('fakultas', 'fakultas.id', '=', 'jurusan.id_fakultas')
                    ->select('name','email','konfirmasi_admin','is_admin','is_dosen','is_mhs',
                             'users.created_at as created_user','mahasiswa.*', 'id_fakultas')
                    ->where('users.id', $idUser)->get();
        }else if($user->is_dosen == 1){
            return  DB::table('users')->leftJoin('dosen','dosen.id_user','=','users.id')
                    ->select('name','email','konfirmasi_admin','is_admin','is_dosen','is_mhs',
                             'users.created_at as created_user','dosen.*')
                    ->where('users.id', $idUser)->get();
        }else{
            return $user = array(
                'is_admin' => 1,
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = Auth::user();
        $idUser = $user->id;
        if($user->is_mhs == 1){
            $request->validate([
                'id_jurusan' => 'required', 
                'id_tahun_ajaran' => 'required', 
                'nama_depan' => 'required', 
                'nama_belakang' => 'required', 
                'nim' => 'required', 
                'alamat' => 'required', 
                'nohp' => 'required',
                'email' => 'required|max:255|email',
            ]);
            
            $mahasiswa = Mahasiswa::where('id', $id)->update([
                'id_jurusan' => $request->id_jurusan, 
                'id_tahun_ajaran' => $request->id_tahun_ajaran, 
                'nama_depan' => $request->nama_depan, 
                'nama_belakang' => $request->nama_belakang, 
                'nim' => $request->nim, 
                'alamat' => $request->alamat, 
                'nohp' => $request->nohp,
            ]);
            
    
            $user = User::where('id', $idUser)->update([
                'name' => $request->nama_depan." ".$request->nama_belakang,
                'email' => $request->email
            ]);
    
            if($mahasiswa && $user){
                return response(200);    
            } else {
                return response(500);    
            }

        }else if($user->is_dosen == 1){
            $request->validate([
                'id_jurusan' => 'required', 
                'id_tahun_ajaran' => 'required', 
                'id_fakultas' => 'required', 
                'nama' => 'required',  
                'nidn' => 'required', 
                'alamat' => 'required', 
                'no_wa' => 'required',
                'email' => 'required|max:255|email',
            ]);
            
            $dosen = Dosen::where('id', $id)->update([
                'id_jurusan' => $request->id_jurusan, 
                'id_tahun_ajaran' => $request->id_tahun_ajaran, 
                'id_fakultas' => $request->id_fakultas, 
                'nama' => $request->nama, 
                'nidn' => $request->nidn, 
                'alamat' => $request->alamat, 
                'no_wa' => $request->no_wa,
            ]);

            $user = User::where('id', $idUser)->update([
                'name' => $request->nama,
                'email' => $request->email
            ]);
    
            if($dosen && $user){
                return response(200);    
            } else {
                return response(500);    
            }
        }else{
            return $user = array(
                'is_admin' => 1,
            );
        }
    }

    public function uploadFoto(Request $request)
    {
        $user = Auth::user();
        $idUser = $user->id;

        if(!$request->get('image')){
            return response(500);
        }

        if($user->is_mhs == 1){
            $dataUser = DB::table('users')->leftJoin('mahasiswa','mahasiswa.id_user','=','users.id')
                        ->select('foto', 'mahasiswa.id')
                        ->where('mahasiswa.id_user', $idUser)->get();
            $idMahasiswa = $dataUser[0]->id;
            if($dataUser[0]->foto){
                File::delete('images/'.$dataUser[0]->foto);
                $file = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/').$name);
                
                $mahasiswa = Mahasiswa::where('id', $idMahasiswa)->update([
                    'foto' => $name,
                ]);
                if($mahasiswa){
                    return response(200);    
                } else {
                    return response(500);    
                }
            }else{
                $file = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/').$name);
                
                $mahasiswa = Mahasiswa::where('id', $idMahasiswa)->update([
                    'foto' => $name,
                ]);
                if($mahasiswa){
                    return response(200);    
                } else {
                    return response(500);    
                }
            }
        }else if($user->is_dosen == 1){
            $dataUser = DB::table('users')->leftJoin('dosen','dosen.id_user','=','users.id')
                        ->select('foto', 'dosen.id')
                        ->where('dosen.id_user', $idUser)->get();
            $idDosen = $dataUser[0]->id;
            if($dataUser[0]->foto){
                File::delete('images/'.$dataUser[0]->foto);
                $file = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/').$name);
                
                $mahasiswa = Dosen::where('id', $idDosen)->update([
                    'foto' => $name,
                ]);
                if($mahasiswa){
                    return response(200);    
                } else {
                    return response(500);    
                }
            }else{
                $file = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/').$name);
                
                $mahasiswa = Dosen::where('id', $idDosen)->update([
                    'foto' => $name,
                ]);
                if($mahasiswa){
                    return response(200);    
                } else {
                    return response(500);    
                }
            }
        }
        

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
