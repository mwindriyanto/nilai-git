<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\SubParameter;
use Illuminate\Support\Facades\Auth;

class SubParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dosenid=Auth::user()->id;
        return DB::table('subparameter')
               ->leftJoin('parameter',
               'subparameter.id_parameter','=','parameter.id')
               ->leftJoin('mata_kuliah','parameter.id_mata_kuliah','=','mata_kuliah.id')
               ->leftJoin('jurusan','mata_kuliah.id_jurusan','=','jurusan.id')
               ->select('subparameter.*','parameter.parameter','parameter.id_dosen','mata_kuliah.mata_kuliah','jurusan.jurusan')
               ->where('parameter.id_dosen',$dosenid)
               ->paginate(10);
    }
    
    public function all()
    {
        //
        return SubParameter::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_parameter' => 'required|numeric',
            'prosentase' => 'required|numeric|max:100',
            'subparameter' => 'required|unique:subparameter,subparameter|max:255',
        ]);
        $subparameter = DB::table('subparameter')->insert($request->all());
    }

    public function search(Request $request){
    //    return SubParameter::where('kode','LIKE',"%$request->q%")    
        // ->orWhere('subparameter','LIKE',"%$request->q%")->paginate(10);
        $dosen=DB::table('dosen')->where('id_user',Auth::user()->id)->first();
        // return $dosen->id;
        return DB::table('subparameter')
               ->leftJoin('parameter','subparameter.id_parameter','=','parameter.id')
               ->leftJoin('mata_kuliah','parameter.id_mata_kuliah','=','mata_kuliah.id')
               ->select('subparameter.*','parameter.parameter','parameter.id_dosen','mata_kuliah.mata_kuliah')
               ->where('parameter.id_dosen',$dosen->id)    
               ->where('mata_kuliah.mata_kuliah','LIKE',"%$request->q%")
               ->paginate(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return DB::table('subparameter')
               ->leftJoin('parameter',
               'subparameter.id_parameter','=','parameter.id')
               ->select('subparameter.*','parameter.parameter as parameter')
               ->where('subparameter.id',$id)
               ->get();
        // return SubParameter::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'id_parameter' => 'required|numeric',
            'prosentase' => 'required|numeric|max:100',
            'subparameter' => 'required|unique:subparameter,subparameter,'.$id.'|max:255',
        ]);
        $subparameter = SubParameter::find($id)->update($request->all());
        if($subparameter) {
           return response(200);
        } else {
           return response(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $subparameter = SubParameter::destroy($id);
        if($subparameter){
          return response(200);
        } else {
          return response(500);    
        }
    }
}
