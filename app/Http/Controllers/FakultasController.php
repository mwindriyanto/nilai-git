<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fakultas;

class FakultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Fakultas::paginate(10);
    }
    
    public function all()
    {
        //
        return Fakultas::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'kode_fak' => 'required|unique:fakultas,kode_fak|max:255',
            'fakultas' => 'required|unique:fakultas,fakultas|max:255',
        ]);
        $fakultas = Fakultas::create($request->all());
    }

    public function search(Request $request){
       return Fakultas::where('kode_fak','LIKE',"%$request->q%")    
        ->orWhere('fakultas','LIKE',"%$request->q%")->paginate(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return Fakultas::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'kode_fak' => 'required|unique:fakultas,kode_fak,'.$id.'|max:255',
            'fakultas' => 'required|unique:fakultas,fakultas,'.$id.'|max:255',
        ]);
        $fakultas = Fakultas::find($id)->update($request->all());
        if($fakultas) {
           return response(200);
        } else {
           return response(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $fakultas = Fakultas::destroy($id);
        if($fakultas){
          return response(200);
        } else {
          return response(500);    
        }
    }
}
