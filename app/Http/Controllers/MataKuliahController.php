<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MataKuliah;
use Illuminate\Support\Facades\Auth;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_user = Auth::user()->id;
        return DB::select('SELECT mk.*, j.jurusan, t.tahun_ajaran FROM mata_kuliah mk
                        LEFT JOIN jurusan j ON j.id = mk.id_jurusan
                        LEFT JOIN tahun_ajaran t ON t.id = mk.id_tahun_ajaran
                        where id_dosen = ?', [$id_user]);
    }

    public function all()
    {
        return MataKuliah::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function search(Request $request){
        return DB::table('mata_kuliah')->join('jurusan','mata_kuliah.id_jurusan','=','jurusan.id')
                ->join('tahun_ajaran', 'mata_kuliah.id_tahun_ajaran','=','tahun_ajaran.id')
                ->select('mata_kuliah.*', 'jurusan', 'tahun_ajaran')
                ->where('mata_kuliah.kode','LIKE', "%$request->q%")
                ->orWhere('mata_kuliah.mata_kuliah','LIKE', "%$request->q%")
                ->paginate(10);
     }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode' => 'required',
            'mata_kuliah' => 'required',
            'id_jurusan' => 'required',
            'id_tahun_ajaran' => 'required',
        ]);
        $id_user = Auth::user()->id;
        MataKuliah::create([
            'kode' => $request->kode,
            'mata_kuliah' => $request->mata_kuliah,
            'id_jurusan' => $request->id_jurusan,
            'id_tahun_ajaran' => $request->id_tahun_ajaran,
            'id_dosen' => $id_user,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mataKuliah = DB::select('SELECT * FROM mata_kuliah mk
                        LEFT JOIN jurusan j ON j.id = mk.id_jurusan
                        LEFT JOIN tahun_ajaran t ON t.id = mk.id_tahun_ajaran
                        LEFT JOIN fakultas f ON f.id = j.id_fakultas
                        where mk.id = ?', [$id]);
        return $mataKuliah;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode' => 'required',
            'mata_kuliah' => 'required',
            'id_jurusan' => 'required',
            'id_tahun_ajaran' => 'required',
        ]);

        $mataKuliah = MataKuliah::where('id', $id)->update([
            'kode' => $request->kode,
            'mata_kuliah' => $request->mata_kuliah,
            'id_jurusan' => $request->id_jurusan,
            'id_tahun_ajaran' => $request->id_tahun_ajaran,
        ]);

        if($mataKuliah){
            return response(200);    
        } else {
            return response(500);    
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mataKuliah = MataKuliah::destroy($id);
        if($mataKuliah){
          return response(200);
        } else {
          return response(500);    
        }
    }
}
