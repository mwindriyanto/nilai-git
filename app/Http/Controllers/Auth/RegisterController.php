<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\TahunAjaran;
use App\Fakultas;
use App\Jurusan;
use App\Dosen;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nidn' => 'required|numeric|unique:dosen',
            'name' => 'required',
            'institusi' => 'required',
            'id_institusi' => 'required',
            'profesi' => 'required',
            'tahun_ajaran' => 'required',
            'fakultas' => 'required',
            'jurusan' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'no_hp' => 'required',
            'alamat' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $validatedData = $request->validate([
            'nidn' => 'required|numeric|unique:dosen',
            'name' => 'required',
            'institusi' => 'required',
            'id_institusi' => 'required',
            'profesi' => 'required',
            'tahun_ajaran' => 'required',
            'fakultas' => 'required',
            'jurusan' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'no_hp' => 'required',
            'alamat' => 'required',
        ]);

        $user=User::create([
            'name'              => $request->name,
            'email'             => $request->email,
            'password'          => hash::make($request->password),
            'konfirmasi_admin'  => 0,
            'is_admin'          => 0,
            'is_dosen'          => 1,
            'is_mhs'            => 0,
            'id_jurusan'        => $request->jurusan,
        ]);

        $dosen= new Dosen;
            $dosen->nidn = $request->nidn;
            $dosen->nama = $request->name;
            $dosen->id_institusi = $request->id_institusi;
            $dosen->id_tahun_ajaran = $request->tahun_ajaran;
            $dosen->id_fakultas =$request->fakultas;
            $dosen->id_jurusan = $request->jurusan;
            $dosen->id_user = $user->id;
            $dosen->no_wa = $request->no_hp;
            $dosen->alamat = $request->alamat;
        $dosen->save();
        return redirect('/login');
    }

    protected function getInstitusi(Request $request)
    {
        if($request->get('institusi'))
        {
            $query=$request->get('institusi');
            $data=DB::table('institusi')->where('institusi','LIKE',"%$query%")->get();
            // return $data;
            $output='<ul class="dropdown-menu" style="display:block; position:relative;">';
            foreach($data as $row)
            {
                $data=$row->id;
                $output.='<li onclick="tempel('.$data.')"><a href="#">'.$row->institusi.'</a></li>';                
            }
            $output.='</ul>';
            echo $output;
        }
    }

    protected function getTahunAjaran()
    {
        $data=TahunAjaran::All();
        $ajaran='<option value="">-Pilih Tahun Ajaran-</option>';
        foreach($data as $data)
        {
            $ajaran.='<option value="'.$data->id.'">'.$data->tahun_ajaran.'</option>';
        }
        echo $ajaran;
    }

    protected function getFakultas()
    {
        $data=Fakultas::All();
        $fakultas='<option value="">-Pilih Fakultas-</option>';
        foreach($data as $data)
        {
            $fakultas.='<option value="'.$data->id.'">'.$data->fakultas.'</option>';
        }
        echo $fakultas;
    }

    protected function getJurusan(Request $request)
    {
        if($request->fak==null || $request->fak==''){
            $jurusan='<option value="">-Pilih Jurusan-</option>';
            echo $jurusan;
        }
        else{
            $data=Jurusan::where('id_fakultas',$request->fak)->get();
            $jurusan='<option value="">-Pilih Jurusan-</option>';
            foreach($data as $data)
            {
                $jurusan.='<option value="'.$data->id.'">'.$data->jurusan.'</option>';
            }
            echo $jurusan;
        }
    }
}
