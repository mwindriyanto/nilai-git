<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubParameter extends Model
{
    protected $table = "subparameter";
    protected $fillable = ['id_parameter','subparameter','prosentase'];
}
