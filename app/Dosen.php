<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    //
    protected $table = "dosen";

    protected $fillable = [
        'nidn',
        'nama',
        'id_tahun_ajaran',
        'id_fakultas',
        'id_jurusan',
        'id_user',
        'no_wa',
        'alamat'
    ];
    
}
