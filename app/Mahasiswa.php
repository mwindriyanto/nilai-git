<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = "mahasiswa";
    protected $fillable = ['id_jurusan', 'id_tahun_ajaran', 'nama_depan', 'nama_belakang', 'nim', 'alamat', 'nohp', 'id_user'];
}
