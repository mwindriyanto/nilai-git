/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.6-MariaDB : Database - assess_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`assess_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `assess_db`;

/*Table structure for table `dosen` */

DROP TABLE IF EXISTS `dosen`;

CREATE TABLE `dosen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(25) DEFAULT NULL,
  `nama` varchar(75) DEFAULT NULL,
  `wa` varchar(15) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dosen` */

/*Table structure for table `fakultas` */

DROP TABLE IF EXISTS `fakultas`;

CREATE TABLE `fakultas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_fak` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fakultas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fakultas_kode_unique` (`kode_fak`),
  UNIQUE KEY `fakultas_fakultas_unique` (`fakultas`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `fakultas` */

insert  into `fakultas`(`id`,`kode_fak`,`fakultas`,`created_at`,`updated_at`) values 
(2,'FTTI','Fakultas Teknik dan Teknologi Infromasi','2019-10-11 12:28:53','2019-10-11 12:28:53'),
(3,'FES','Fakultas Ekonomi dan Sosial','2019-10-11 12:29:16','2019-10-11 12:29:16'),
(4,'FKES','Fakultas Kesehatan','2019-10-11 12:29:36','2019-10-11 12:29:36'),
(6,'FKIP','Fakultas Ilmu Pendidikan','2019-12-11 05:30:24','2019-12-11 05:30:24');

/*Table structure for table `general_settings` */

DROP TABLE IF EXISTS `general_settings`;

CREATE TABLE `general_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `general_settings` */

/*Table structure for table `jurusan` */

DROP TABLE IF EXISTS `jurusan`;

CREATE TABLE `jurusan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_fakultas` int(10) unsigned NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `jurusan_kode_unique` (`kode`),
  UNIQUE KEY `jurusan_jurusan_unique` (`jurusan`),
  KEY `jurusan_id_fakultas_foreign` (`id_fakultas`),
  CONSTRAINT `jurusan_id_fakultas_foreign` FOREIGN KEY (`id_fakultas`) REFERENCES `fakultas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `jurusan` */

insert  into `jurusan`(`id`,`id_fakultas`,`kode`,`jurusan`,`created_at`,`updated_at`) values 
(1,2,'IF','Teknik Informatika','2019-12-11 13:12:37',NULL),
(2,2,'SI','Sistem Informasi','2019-12-11 13:12:41',NULL),
(3,2,'TI','Teknologi Informasi','2019-12-11 13:12:43',NULL),
(4,2,'MI','Manajemen Informatika','2019-12-11 13:12:45',NULL),
(5,3,'AK','Akutansi','2019-12-11 13:12:47',NULL),
(6,4,'KB','Kebidanan','2019-12-11 13:12:50',NULL),
(7,4,'PR','Keperawatan','2019-12-11 13:12:52',NULL);

/*Table structure for table `konten` */

DROP TABLE IF EXISTS `konten`;

CREATE TABLE `konten` (
  `konten` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `konten` */

insert  into `konten`(`konten`) values 
('<p style=\"text-align:center\"><span style=\"font-size:20px\">Selamat Datang di Sistem Informasi Nilai Akademi</span>k</p>\r\n\r\n<p style=\"text-align:center\">Tahun Ajaran 2014/2015</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:28px\"><strong>Program Diploma</strong></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:28px\"><strong>Institut Pertanian Bogor</strong></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n');

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_jurusan` int(10) unsigned NOT NULL,
  `id_tahun_ajaran` int(10) unsigned NOT NULL,
  `nama_depan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_belakang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nohp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mahasiswa_nim_unique` (`nim`),
  KEY `mahasiswa_id_jurusan_foreign` (`id_jurusan`),
  KEY `mahasiswa_id_tahun_ajaran_foreign` (`id_tahun_ajaran`),
  CONSTRAINT `mahasiswa_id_jurusan_foreign` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id`),
  CONSTRAINT `mahasiswa_id_tahun_ajaran_foreign` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `mahasiswa` */

/*Table structure for table `mata_kuliah` */

DROP TABLE IF EXISTS `mata_kuliah`;

CREATE TABLE `mata_kuliah` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_jurusan` int(10) unsigned NOT NULL,
  `id_tahun_ajaran` int(10) unsigned NOT NULL,
  `id_dosen` int(10) unsigned NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mata_kuliah` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mata_kuliah_kode_unique` (`kode`),
  KEY `mata_kuliah_id_jurusan_foreign` (`id_jurusan`),
  KEY `mata_kuliah_id_dosen_foreign` (`id_dosen`),
  CONSTRAINT `mata_kuliah_id_dosen_foreign` FOREIGN KEY (`id_dosen`) REFERENCES `users` (`id`),
  CONSTRAINT `mata_kuliah_id_jurusan_foreign` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `mata_kuliah` */

/*Table structure for table `matkul_subparam_setting` */

DROP TABLE IF EXISTS `matkul_subparam_setting`;

CREATE TABLE `matkul_subparam_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_matkul` int(10) unsigned NOT NULL,
  `id_sub_parameter` int(10) unsigned NOT NULL,
  `id_tahun_ajaran` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `matkul_subparam_setting_id_matkul_foreign` (`id_matkul`),
  KEY `matkul_subparam_setting_id_sub_parameter_foreign` (`id_sub_parameter`),
  KEY `matkul_subparam_setting_id_tahun_ajaran_foreign` (`id_tahun_ajaran`),
  CONSTRAINT `matkul_subparam_setting_id_matkul_foreign` FOREIGN KEY (`id_matkul`) REFERENCES `mata_kuliah` (`id`),
  CONSTRAINT `matkul_subparam_setting_id_sub_parameter_foreign` FOREIGN KEY (`id_sub_parameter`) REFERENCES `subparameter` (`id`),
  CONSTRAINT `matkul_subparam_setting_id_tahun_ajaran_foreign` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `matkul_subparam_setting` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_parent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `menu` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_10_08_000358_create_fakultas_table',1),
(4,'2019_10_08_000542_create_jurusan_table',1),
(5,'2019_10_08_005129_create_mata_kuliah_table',1),
(6,'2019_10_08_005437_create_tahun_ajaran_table',1),
(7,'2019_10_08_023739_create_parameter_nilai_table',1),
(8,'2019_10_08_023949_create_sub_parameter_nilai_table',1),
(9,'2019_10_08_024315_create_mahasiswa_table',1),
(10,'2019_10_08_025042_create_matkul_subparam_setting_table',1),
(11,'2019_10_08_030009_create_nilai_mahasiswa_table',1),
(12,'2019_10_08_043142_create_general_settings_table',1),
(13,'2019_10_08_043528_create_menu_table',1),
(14,'2019_10_08_043754_create_permission_settings_table',1);

/*Table structure for table `nilai_mahasiswa` */

DROP TABLE IF EXISTS `nilai_mahasiswa`;

CREATE TABLE `nilai_mahasiswa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tahun_ajaran` int(10) unsigned NOT NULL,
  `id_mahasiswa` int(10) unsigned NOT NULL,
  `id_mss` int(10) unsigned NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nilai_mahasiswa_id_tahun_ajaran_foreign` (`id_tahun_ajaran`),
  KEY `nilai_mahasiswa_id_mahasiswa_foreign` (`id_mahasiswa`),
  KEY `nilai_mahasiswa_id_mss_foreign` (`id_mss`),
  CONSTRAINT `nilai_mahasiswa_id_mahasiswa_foreign` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`),
  CONSTRAINT `nilai_mahasiswa_id_mss_foreign` FOREIGN KEY (`id_mss`) REFERENCES `matkul_subparam_setting` (`id`),
  CONSTRAINT `nilai_mahasiswa_id_tahun_ajaran_foreign` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `nilai_mahasiswa` */

/*Table structure for table `parameter` */

DROP TABLE IF EXISTS `parameter`;

CREATE TABLE `parameter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parameter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prosentase` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `parameter` */

insert  into `parameter`(`id`,`parameter`,`prosentase`,`created_at`,`updated_at`) values 
(1,'UTS',20,'2020-01-04 06:13:02','2020-01-04 06:20:40'),
(2,'UAS',20,'2020-01-04 06:20:13','2020-01-04 06:20:13'),
(3,'Proyek Akhir',30,'2020-01-04 06:20:55','2020-01-04 06:20:55'),
(4,'Presensi',5,'2020-01-04 06:21:14','2020-01-04 06:24:30'),
(5,'Tugas',10,'2020-01-04 06:22:03','2020-01-04 06:22:03'),
(6,'Presentasi',15,'2020-01-04 06:24:22','2020-01-04 06:24:22');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_settings` */

DROP TABLE IF EXISTS `permission_settings`;

CREATE TABLE `permission_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_menu` int(10) unsigned NOT NULL,
  `is_allowed` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_user` int(10) unsigned NOT NULL,
  `id_group_user` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_settings_id_menu_foreign` (`id_menu`),
  KEY `permission_settings_id_user_foreign` (`id_user`),
  CONSTRAINT `permission_settings_id_menu_foreign` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`),
  CONSTRAINT `permission_settings_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_settings` */

/*Table structure for table `subparameter` */

DROP TABLE IF EXISTS `subparameter`;

CREATE TABLE `subparameter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parameter` int(10) unsigned NOT NULL,
  `subparameter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prosentase` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_parameter_nilai_id_parameter_foreign` (`id_parameter`),
  CONSTRAINT `sub_parameter_nilai_id_parameter_foreign` FOREIGN KEY (`id_parameter`) REFERENCES `parameter` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `subparameter` */

insert  into `subparameter`(`id`,`id_parameter`,`subparameter`,`prosentase`,`created_at`,`updated_at`) values 
(1,3,'Progres',30,NULL,NULL),
(2,3,'Pengujian',30,NULL,NULL),
(3,3,'Hasil',40,NULL,NULL),
(4,5,'Tugas 1',30,NULL,NULL),
(5,5,'Tugas 2',30,NULL,NULL),
(6,5,'Tugas 3',40,NULL,NULL);

/*Table structure for table `tahun_ajaran` */

DROP TABLE IF EXISTS `tahun_ajaran`;

CREATE TABLE `tahun_ajaran` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tahun_ajaran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tahun_ajaran_tahun_ajaran_unique` (`tahun_ajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tahun_ajaran` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `konfirmasi_admin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_admin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_dosen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `is_mhs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_jurusan` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`username`,`email`,`password`,`konfirmasi_admin`,`is_admin`,`is_dosen`,`is_mhs`,`id_jurusan`,`remember_token`,`created_at`,`updated_at`) values 
(21,'Wahyu',NULL,'mwindriyanto@gmail.com','$2y$10$fsQ1ZPoZNjqlcbwNXVm7pOERdwBnIrTRgPRec0ECJG8n1XRVXKoVq','1','0','1','0',0,'EKSwFhaPbM1zIKOSZXFa8bOMq9gF6K87a98LNW7qFszdUMupjG1BZC4Yq5Sl','2019-10-11 11:56:40','2019-10-11 11:56:40');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
